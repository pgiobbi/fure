import time
import json
import logging
import logging.config
from fure import __LOGGERPATH__

try:
    from colorama import Fore, Back, Style
    COLOR_AVAIL = True
except Exception as e:
    COLOR_AVAIL = False


def color_back_success(orig):
    """Color a string with a green color."""
    if not COLOR_AVAIL:
        return orig
    return f'{Back.GREEN}{orig}{Style.RESET_ALL}'


def color_fore_success(orig):
    """Color a string with a red color."""
    if not COLOR_AVAIL:
        return orig
    return f'{Fore.GREEN}{orig}{Style.RESET_ALL}'


def color_back_warning(orig):
    """Color a string with a green color."""
    if not COLOR_AVAIL:
        return orig
    return f'{Back.YELLOW}{orig}{Style.RESET_ALL}'


def color_fore_warning(orig):
    """Color a string with a red color."""
    if not COLOR_AVAIL:
        return orig
    return f'{Fore.YELLOW}{orig}{Style.RESET_ALL}'


def color_back_danger(orig):
    """Color a string with a green color."""
    if not COLOR_AVAIL:
        return orig
    return f'{Back.RED}{orig}{Style.RESET_ALL}'


def color_fore_danger(orig):
    """Color a string with a red color."""
    if not COLOR_AVAIL:
        return orig
    return f'{Fore.RED}{orig}{Style.RESET_ALL}'


# TODO fix paths etc
with open(__LOGGERPATH__) as fp:
    logconf = json.load(fp)

logging.config.dictConfig(logconf)
logging.Formatter.converter = time.gmtime
logging.addLevelName(logging.WARNING, 'WARN')

# create logger
logger = logging.getLogger('fure')
logger.info('logger initialized')

__all__ = (
    logger,
    color_back_success,
    color_fore_success,
    color_back_warning,
    color_fore_warning,
    color_back_danger,
    color_fore_danger
)

"""
Credentials must be a json obejct

"""
import os
import json
from cryptography.fernet import Fernet
from fure.utils.log import logger


class CredentialStorage():
    def __init__(self, path, key=None):
        self.path = path
        self.key = key
        self.pkey = None
        self.address = None
        self.load_credentials()

    def encrypt_file(self, encrypted_path=None):
        """Encrypt a file, returning the encryption key."""
        if self.encrypted:
            raise ValueError('File is already encrypted')
        key = Fernet.generate_key()
        fernet = Fernet(key)

        with open(self.path, 'rb') as fp:
            original = fp.read()

        encrypted = fernet.encrypt(original)

        dirname = os.path.dirname(self.path)
        base, extension = os.path.splitext(self.path)

        if encrypted_path is None:
            encrypted_path = base + '_encrypted' + extension
        with open(encrypted_path, 'wb') as fp:
            fp.write(encrypted)

        logger.info(f'{encrypted_path} {key}')

    def load_credentials(self):
        """Load credentials from json file."""
        if not self.encrypted:
            with open(self.path) as fp:
                data = json.load(fp)
        else:
            with open(self.path, 'rb') as fp:
                encrypted = fp.read()
            data = json.loads(Fernet(self.key).decrypt(encrypted))
        self.pkey = data.get('chicco')
        self.address = data.get('adamo')


    @property
    def encrypted(self):
        return False if self.key is None else True

from fure.blockchains.base.explorer import BaseExplorer

class EthExplorer(BaseExplorer):
    def __init__(self, api_key=None, api_url=None, explorer_url=None):
        self.symbol = 'ETH'
        self.api_key = api_key
        self.api_url = api_url if api_url is not None \
            else 'https://api.etherscan.io/'

        self.explorer_url = explorer_url if explorer_url is not None \
            else 'https://etherscan.io/'
                
        self.api_url = self.api_url.rstrip('/')
        self.explorer_url = self.explorer_url.rstrip('/')
        super().__init__()

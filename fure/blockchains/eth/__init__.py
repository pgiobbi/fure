import os
from fure import __CONFPATH__
from fure.blockchains.eth.explorer import EthExplorer


here = os.path.dirname(os.path.abspath(__file__))
foldername = os.path.basename(here)

__ABIPATH__ = os.path.join(__CONFPATH__, 'abi', foldername)

__all__ = [__ABIPATH__, EthExplorer]

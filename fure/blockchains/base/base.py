import os
import json
from time import sleep
from glob import glob
from web3 import Web3
from fure.utils import logger
from fure.utils.log import (
    color_back_warning, color_fore_warning,
    color_back_success, color_fore_success
)


class BaseBlockchainConnector:
    def __init__(self, credentials=None):
        """Initialize the class."""
        self.init_w3()
        self.credentials = credentials
        self.contracts = {}

    def preload_all_contracts(self):
        """Preload all contracts in memory."""
        paths = glob(os.path.join(self.__ABIPATH__, '*.json'))
        logger.info(paths)
        for path in paths:
            self.load_contract(path)

    def fetch_owned_tokens(self, address=None):
        """Fetch tokens owned by address and save their abi if available."""
        address = address if address is not None else self.address
        if address is None:
            raise ValueError('Address is None')
        if self.explorer is None:
            raise ValueError('Explorer is None')

        addresses = self.explorer.get_token_holdings(address)

        logger.info(f'Found {len(addresses)} tokens for {address}')
        for address in addresses:
            addr_ = self.w3.toChecksumAddress(address)
            abi = self.explorer.get_abi(address)
            contract = self.w3.eth.contract(address=addr_, abi=abi)
            symbol = contract.functions.symbol().call()
            obj = {
                'address': addr_,
                'abi': json.loads(abi)
            }
            self.save_contract(symbol, obj)

    def init_w3(self):
        """Initialize the Web3 library. Use wss by preference."""
        try:
            self.w3 = Web3(Web3.WebsocketProvider(self.rpc_wss))
            assert not self.w3.isConnected()
        except Exception as e:
            logger.error(f'Connection via websocket {self.rpc_wss} failed. '
                f'Reason: {e}')
            self.w3 = Web3(Web3.HTTPProvider(self.rpc_url))

    def load_contract(self, contract_path):
        """Load a contract in memory."""
        with open(contract_path) as fp:
            data = json.load(fp)

        fname = os.path.basename(os.path.splitext(contract_path)[0])

        self.contracts[fname] = self.w3.eth.contract(**data)
        logger.info(f'Contract {contract_path} - {fname.upper()} Loaded')

    def save_contract(self, symbol, obj):
        """Save a contract in storage."""
        path = os.path.join(self.__ABIPATH__, f'{symbol.lower()}.json')
        os.makedirs(self.__ABIPATH__, exist_ok=True)
        with open(path, 'w') as fp:
            json.dump(obj, fp, indent=2)
        logger.info(f'Saved {path}')

    def get_nonce(self):
        """Get the nonce for the current address."""
        return self.w3.eth.get_transaction_count(self.address)

    def get_block_number(self):
        """Return the current block number."""
        return self.w3.eth.block_number

    def get_balance(self, human=False):
        """Get native coin balance."""
        res = self.w3.eth.get_balance(self.address)
        if human:
            return f'{self.w3.fromWei(res, "ether")} {self.symbol.upper()}'
        return res

    def get_token_balance(self, symbol, human=False):
        """Get the given token balance for the current address."""
        contract = self.get_contract(symbol)
        res = contract.functions.balanceOf(self.address).call()
        if human:
            return f'{self.w3.fromWei(res, "ether")} {symbol.upper()}'
        return res

    def get_mandatory_fields(self):
        """Generate mandatory fields to build a transaction."""
        nonce = self.get_nonce()
        fields = {
            'from': self.address,
            'nonce': nonce,
        }
        return fields

    def send_token(self, symbol, recipient, amount):
        """Transfer an ERC20 token."""
        contract = self.get_contract(symbol)
        if amount == 'max':
            amount = self.get_token_balance(symbol)
            logger.info(f'Amount max -> Max balance is '
                f'{self.w3.fromWei(amount, "ether")} {symbol.upper()}')

        tx = contract.functions.transfer(
            recipient,
            amount
        ).buildTransaction({
            **self.get_mandatory_fields(),
            'value': 0
        })
        logger.info(tx)

        data = tx.get('data')
        if data is not None:
            decoded = contract.decode_function_input(data)
            logger.info(f'Decoded input data: {decoded}')
        signed_tx = self.sign_tx(tx)
        token_hex = self.send_signed_tx(signed_tx)
        logger.info(color_back_success(
            f'Sent transaction tx_token: {self.w3.toHex(token_hex)}'))
        return token_hex

    def sign_tx(self, tx, pkey=None):
        """Sign a transaction with the private key."""
        if pkey is None:
            pkey = self.pkey
        return self.w3.eth.account.sign_transaction(tx, private_key=pkey)

    def send_signed_tx(self, signed_tx):
        """Send a signed transaction to the blockchain."""
        return self.w3.eth.send_raw_transaction(signed_tx.rawTransaction)

    def get_contract(self, symbol):
        """Returns the contract fot the symbol. Error otherwise."""
        contract = self.contracts.get(symbol)
        if contract is None:
            raise ValueError(f'Contract for "{symbol}" not found!')
        return contract

    def watch_for_balance_changes(self, sleep_time=1, symbol=None,
        trigger_fn=None, trigger_kwargs=None, trigger_after=None
    ):
        """Watch for symbol balance change; watch coin if None."""
        # TODO: watch list of tokens
        if symbol is None:
            get_balance = self.get_balance
            args = ()
            symbol_human = self.symbol.upper()
        else:
            get_balance = self.get_token_balance
            args = (symbol, )
            symbol_human = symbol.upper()

        logger.info('=' * 20 + f'> Starting watching strategy on {symbol_human} '
            f'balance for {self.address} <' + '=' * 20)

        old_balance = get_balance(*args)
        old_block_num = self.get_block_number()
        execute_trigger = True
        while True:
            new_block_num = self.get_block_number()
            if new_block_num == old_block_num:
                continue

            new_balance = get_balance(*args)
            logger.info(
                f'[BlockNum {new_block_num}] '
                f'{symbol_human} Balance: {new_balance} wei'
            )
            if new_balance != old_balance:
                # Perform the given action if this condition triggers
                logger.warning(
                    f'[BlockNum {new_block_num}] ' +
                    color_back_warning(
                        f'{symbol_human} Balance changed from {old_balance} '
                        f'wei to {new_balance} wei!'
                ))

                if trigger_fn is not None:
                    # Handle trigger_after = 'skip'
                    if not execute_trigger:
                        logger.warning(
                            f'[BlockNum {new_block_num}] ' +
                            color_fore_warning(
                                f'{trigger_fn} not fired for skip policy! '
                                'Triggering from the next event'))
                        execute_trigger = True
                    else:
                        logger.info(
                            f'[BlockNum {new_block_num}] ' +
                            color_fore_success(
                                f'Calling {trigger_fn} {trigger_kwargs}'
                        ))
                        trigger_fn(**trigger_kwargs)

                        # Decide the behavior after the event triggered
                        if trigger_after == 'break':
                            return
                        elif trigger_after == 'skip':  # Skip next event + continue
                            execute_trigger = False


            old_balance = new_balance
            old_block_num = new_block_num
            sleep(sleep_time)

    @property
    def address(self):
        """Getter for the account address."""
        if self.credentials is None:
            return None
        return self.credentials.address

    @property
    def pkey(self):
        """Getter for the private key of the current account."""
        if self.credentials is None:
            return None
        return self.credentials.pkey

import re
import time
import functools
import requests as r
from fure.utils import logger
from web3.auto import w3
from lxml import etree


class BaseExplorer:
    """
    Base class for blockchain explorers.

    The explorer for the base class is etherscan.io or any of its clones.
    Child classes can override this if necessary.

    """
    def __init__(self):
        self.w3 = w3
        self._last_calls = []   # Timestamps for throttling

        # Fake postman headers to trick etherscan to accept the request
        # They otherwise seem to reject requests made from python scripts
        self._headers = {
            'User-Agent': 'PostmanRuntime/7.29.0',
            'Accept': '*/*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
        }

    def required(field):
        """Define a decorator that checks that a property is defined."""
        def inner_decorator(func):
            @functools.wraps(func)
            def required_decorator(self, *args, **kwargs):
                try:
                    value = self.__getattribute__(field)
                except AttributeError:
                    raise AttributeError(f'Required field "{field}" missing')
                if value is None:
                    raise ValueError(f'Required field "{field}" is None')
                return func(self, *args, **kwargs)
            return required_decorator
        return inner_decorator

    def rate_limiter(requests_per_second=5):
        """
        Define a decotator to throttle the requests to the max API limit.

        By default Etherscan accepts max 5 requests per second. This means
        that there must be at least 1 full second between the current request
        and 5th previous request. The necessary sleep is induced if this is not
        the case.

        """
        def inner_decorator(func):
            @functools.wraps(func)
            def rate_limit_decorator(self, *args, **kwargs):
                now = time.time()
                try:
                    delta = now - self._last_calls[0]
                    if delta < 1:
                        logger.debug(f'Throttling for {1 - delta} seconds')
                        time.sleep(1 - delta)
                    now = time.time()  # Overwrite time after sleep
                except Exception as e:
                    pass

                self._last_calls = (self._last_calls + [now])[-requests_per_second:]
                return func(self, *args, **kwargs)

            return rate_limit_decorator
        return inner_decorator

    def _send_get_request(self, url):
        """Internal method to send get requests to the explorer."""
        try:
            res = r.get(url, headers=self._headers)
            if res.status_code != 200:
                raise ValueError(f'Status code is {res.status_code} != 200')
            message = res.json().get('message')
            if message != 'OK':
                logger.error(f'NOK: {res.json()}')
                raise ValueError(f'Message is "{message}" != "OK"')
            return res.json().get('result')
        except Exception as e:
            logger.error(f'Could not call {url} for {address}. Reason: {e}')
            return None

    @required('api_key')
    @rate_limiter(5)
    def get_balance(self, address, human=False):
        """Get the native token balance in wei for the given address."""
        url = (
            f'{self.api_url}/api'
            '?module=account'
            '&action=balance'
            f'&address={address}'
            '&tag=latest'
            f'&apikey={self.api_key}'
        )
        res = int(self._send_get_request(url))
        if human:
            return f'{self.w3.fromWei(res, "ether")} {self.symbol.upper()}'
        return res

    @required('api_key')
    @rate_limiter(5)
    def get_abi(self, address):
        """Get the abi for the provided contract address and save it."""
        url = (
            f'{self.api_url}/api'
            '?module=contract'
            '&action=getabi'
            f'&address={address}'
            f'&apikey={self.api_key}'
        )
        return self._send_get_request(url)

    @rate_limiter(1)
    def get_token_holdings(self, address):
        """Get the contract addresses for the tokens held by the account."""
        url = f'{self.explorer_url}/address/{address}'
        patt = r'.*token/(.*)\?.*'
        res = r.get(url, headers=self._headers)

        dom = etree.HTML(res.text)
        urls = dom.xpath('//li[contains(@class, "list-custom")]/a/@href')

        groups = [re.match(patt, url) for url in urls]
        return [elem[1] for elem in groups if elem is not None]

from web3.middleware import geth_poa_middleware
from fure.blockchains.base.base import BaseBlockchainConnector
from fure.blockchains.rinkeby import __ABIPATH__


class RinkebyConnector(BaseBlockchainConnector):
    def __init__(self, credentials=None, infura_id=None, explorer=None):
        if infura_id is None:
            infura_id = '9aa3d95b3bc440fa88ea12eaa4456161'

        self.blockchain = 'rinkeby eth'
        self.rpc_url = f'https://rinkeby.infura.io/v3/{infura_id}'
        self.rpc_wss = f'wss://rinkeby.infura.io/v3/{infura_id}'
        self.explorer = explorer
        self.chain_id = 4
        self.symbol = 'ETH'

        self.__ABIPATH__ = __ABIPATH__
        super().__init__(credentials)
        self.w3.middleware_onion.inject(geth_poa_middleware, layer=0)

        self.preload_all_contracts()

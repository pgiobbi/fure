from fure.blockchains.base.base import BaseBlockchainConnector
from fure.blockchains.cronos import __ABIPATH__


class CronosConnector(BaseBlockchainConnector):
    def __init__(self, credentials=None, explorer=None):
        self.blockchain = 'cronos'
        self.rpc_url = 'https://evm-cronos.crypto.org/'
        self.rpc_wss = None
        self.explorer = explorer
        self.chain_id = 25
        self.symbol = 'CRO'

        self.__ABIPATH__ = __ABIPATH__
        super().__init__(credentials)
        self.preload_all_contracts()

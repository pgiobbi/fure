from web3.middleware import geth_poa_middleware
from fure.blockchains.base.base import BaseBlockchainConnector
from fure.blockchains.bsc import __ABIPATH__


class BscConnector(BaseBlockchainConnector):
    def __init__(self, credentials=None, explorer=None):
        self.blockchain = 'bsc'
        self.rpc_url = 'https://bsc-dataseed1.binance.org/'
        self.rpc_wss = None
        self.explorer = explorer
        self.chain_id = 56
        self.symbol = 'BNB'

        self.__ABIPATH__ = __ABIPATH__
        super().__init__(credentials)
        self.w3.middleware_onion.inject(geth_poa_middleware, layer=0)

        self.preload_all_contracts()

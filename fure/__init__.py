import os
import fure

here = os.path.dirname(os.path.abspath(__file__))

__CONFPATH__ = os.path.join(
    os.path.dirname(here),
    'conf'
)

__LOGGERPATH__ = os.path.join(
    __CONFPATH__,
    'logging.json'
)

__all__ = [__CONFPATH__, __LOGGERPATH__]

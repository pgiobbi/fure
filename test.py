import os
from time import sleep
from fure.utils import CredentialStorage, logger
from fure.blockchains.explorers import EthExplorer, RinkebyExplorer, CronosExplorer
from fure.blockchains.connectors import EthConnector, RinkebyConnector, CronosConnector
from lxml import etree

HACKED_CREDS = '/data/projects/crypto/conf/peppino_testnet_hacked_encrypted.json'
SAFE_CREDS = '/data/projects/crypto/conf/peppino_testnet_safe_encrypted.json'

HACKED_KEY = os.environ.get('HACKED_KEY')
SAFE_KEY = os.environ.get('SAFE_KEY')

SLEEP_TIME = 1

hacked_credentials = CredentialStorage(HACKED_CREDS, key=HACKED_KEY)
safe_credentials = CredentialStorage(SAFE_CREDS, key=SAFE_KEY)

random_guy = CredentialStorage(SAFE_CREDS, key=SAFE_KEY)

explorers = (
    CronosExplorer(os.environ.get('CRONOSCAN_APIKEY')),
    EthExplorer(os.environ.get('ETHERSCAN_APIKEY')),
    RinkebyExplorer(os.environ.get('ETHERSCAN_APIKEY'))
)

bots = (
    CronosConnector(credentials=hacked_credentials, explorer=explorers[0]),
    EthConnector(credentials=hacked_credentials, explorer=explorers[1]),
    RinkebyConnector(credentials=hacked_credentials, explorer=explorers[2])
)

# logger.info(f'Bot address: {bot.address}')
# logger.info(f'Bot balance   (From RPC): {bot.get_balance(human=True)}')
# logger.info(f'Bot balance  (Etherscan): {explorer.get_balance(bot.address, human=True)}')

# logger.info(f'LINK balance  (From RPC): {bot.get_token_balance("link", human=True)}')
# logger.info(f'LINK balance (Etherscan): {bot.get_token_balance("link", human=True)}')

# TODO: implement async

# bot.watch_for_balance_changes(
#     sleep_time=SLEEP_TIME, symbol=None,
#     trigger_fn=bot.send_token,
#     trigger_kwargs=dict(
#         symbol='link',
#         recipient=safe_credentials.address,
#         amount='max'
#     ),
#     trigger_after='skip'
# )

addresses = explorer.get_token_holdings(TEST_ADDRESS)
